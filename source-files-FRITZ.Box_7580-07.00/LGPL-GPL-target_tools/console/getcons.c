/*
 *   getcons - redirect console output to current tty
 *   Copyright (C) 2004  Enrik Berkhan <Enrik.Berkhan@inka.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <stdio.h>

int main(int argc, char **argv) {
	int con;
	const char *device = "/dev/tty";

	if (argc != 1 && argc != 2) {
		fprintf(stderr, "usage: getcons\n\tre-direct system console to current tty\n");
		return 1;
	}
	if (argc == 2) {
		device = argv[1];
	}
	if ((con = open(device, O_RDWR)) == -1) {
		perror("open /dev/tty");
		return 1;
	}
	if (-1 == ioctl(con, TIOCCONS)) {
		perror("ioctl TIOCCONS");
		return 1;
	}
	return 0;
}

