/* 
   This structure is used by lib/interfaces.c to return the list of network
   interfaces on the machine
*/

#ifndef _INTERFACES_H
#define _INTERFACES_H

#include "lib/replace/replace.h"
#include "lib/replace/system/network.h"

#define MAX_INTERFACES 128


/*
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
*/

//#include "samba-3.3.0.h"


struct iface_struct {
	char name[16];
	int flags;
	struct sockaddr_storage ip;
	struct sockaddr_storage netmask;
	struct sockaddr_storage bcast;
};


bool make_netmask(struct sockaddr_storage *pss_out,
	const struct sockaddr_storage *pss_in,
	unsigned long masklen);
void make_bcast(struct sockaddr_storage *pss_out,
	const struct sockaddr_storage *pss_in,
	const struct sockaddr_storage *nmask);
void make_net(struct sockaddr_storage *pss_out,
	const struct sockaddr_storage *pss_in,
	const struct sockaddr_storage *nmask);
int get_interfaces(struct iface_struct *ifaces, int max_interfaces);

#endif


