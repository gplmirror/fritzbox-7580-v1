#pragma once

#include <linux/skbuff.h>
#include <linux/avm_pa.h>
#include <linux/avm_pa_hw.h>
#include <linux/compiler.h>

struct netif_info;
typedef struct {
	struct avm_pa_session *pa_session;
	/*
	 * these interfaces might be setup by build_netif_tree()
	 * and will carry information about virtual datapipe interfaces
	 * like pppoe, vlan etc.
	 */
	struct netif_info *ingress_vitf;
	struct netif_info *egress_vitf;

	/*
	 * ppa_session_add expects successive calls with different states. We
	 * emulate those in the adaption layer by hinting them here, as
	 * pa_session contains a more global view.
	 */
	enum {
		PPA_BUF_PREROUTING,
		PPA_BUF_POSTROUTING,
	} state;
	uint32_t post_routing_flags;
} PPA_BUF;

typedef enum e_HDR_DIRECTION {
	HDR_INGRESS,
	HDR_EGRESS,
	HDR_BOTH
} HDR_DIRECTION;

typedef struct avm_pa_session PPA_SESSION;
typedef struct {
	/* resemble nf_conntrack_tuple for compatibility with
	 * ppa_api_sess_helper.c*/
	struct {
		union {
			__be32 all[4];
		} u3;
		union {
			__be16 all;
		} u;
		uint8_t protonum;
		unsigned short l3num;
	} src, dst;
} PPA_TUPLE;

extern int32_t (*build_netif_tree_hook)(PPA_BUF *);
extern void (*reset_netif_tree_hook)(void);

void ppa_if_force_remove(char *ifname);
bool possible_lro_session(struct avm_pa_session *session);

/*TODO CCB: Stimmt das?! */
#define ppa_skb_len(ppa_buf) ppa_buf->pa_session->ingress.pktlen
#define ppa_skb_set_mac_header(ppa_buf, hdr_offset)
#define ppa_skb_reset_mac_header(ppa_buf)

struct net_device* ppa_get_dslite_phyif(struct net_device *netif);

#define PPA_REALTIME_CLASSID 8

static inline int32_t ppa_is_pkt_host_output(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
static inline int32_t ppa_is_pkt_broadcast(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
static inline int32_t ppa_is_pkt_loopback(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
static inline int32_t ppa_is_tcp_established(PPA_SESSION *ppa_session __attribute__((unused)))
{
	/* AVM PA handles this */
	return true;
}

static inline int32_t ppa_is_tcp_open(PPA_SESSION *ppa_session)
{
	BUG(); /* unused */
	return 0;
}

uint32_t ppa_avm_get_vap(PPA_BUF *ppa_buf);

uint32_t ppa_get_pkt_priority(PPA_BUF *skb);
uint32_t ppa_get_skb_mark(PPA_BUF *ppa_buf);
uint32_t ppa_get_skb_extmark(PPA_BUF *ppa_buf);
uint32_t ppa_set_pkt_priority(PPA_BUF *ppa_buf, uint32_t new_pri);
