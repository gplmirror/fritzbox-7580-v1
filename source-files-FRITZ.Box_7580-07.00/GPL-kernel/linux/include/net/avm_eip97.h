#ifndef _AVM_EIP97_H
#define _AVM_EIP97_H

#include <net/xfrm.h>
#include <crypto/ltq_ipsec_ins.h>

//NOTE: direction is either LTQ_SAB_DIRECTION_OUTBOUND or LTQ_SAB_DIRECTION_INBOUND
void avm_eip97_add_sa(struct xfrm_state *xfrm_sa, int direction, unsigned int packetlength);
void avm_eip97_del_sa(struct xfrm_state *xfrm_sa);
int esp_output_accel(struct xfrm_state *x, struct sk_buff *skb);
int esp_input_accel(struct xfrm_state *x, struct sk_buff *skb);

//Information structures which avm_eip97_add_sa passes down to the dedicated decode/encode thread:

/*!
  \brief This is the structure for EIP97 Command Descriptor DW0
 */
struct e97_cdw_0	{
	uint32_t acd_size: 8;	/*!< additional control data size */
	uint32_t first_seg: 1;	/*!< first segment indicator */
	uint32_t last_seg: 1;	/*!< last segment indicator */
	uint32_t res: 5;			/*!< reserved */
	uint32_t buf_size: 17;	/*!< size of input data buffer in bytes */
};

/*!
  \brief This is the structure for EIP97 Command Descriptor DW1
 */
struct e97_cdw_1 {
	uint32_t buf_ptr;	/*!< data buffer pointer */
};

/*!
  \brief This is the structure for EIP97 Command Descriptor DW2
 */
struct e97_cdw_2 {
	uint32_t acd_ptr;	/*!< additional control data pointer */
};

/*!
  \brief This is the structure for EIP97 Command Descriptor DW3
 */
struct e97_cdw_3	{
	uint32_t type: 2;	/*!< type of token, reserved*/
	uint32_t u: 1;		/*!< upper layer header from token */
	uint32_t iv: 3;	/*!< IV??? */
	uint32_t c: 1;		/*!< context control words present in token */
	uint32_t too: 3;	/*!< type of output */
	uint32_t rc: 2;	/*!< reuse context */
	uint32_t ct: 1;	/*!< context type, reserved */
	uint32_t cp: 1;	/*!< context pointer 64bit */
	uint32_t ip: 1;	/*!< EIP97 Mode */
	uint32_t len: 17;	/*!< input packet length */
};

/*!
  \brief This is the structure for EIP97 Command Descriptor DW4
 */
struct e97_cdw_4 {
	uint32_t res: 8;		/*!< reserved */
	uint32_t app_id: 24;	/*!< application id */
};

/*!
  \brief This is the structure for EIP97 Command Descriptor DW5
 */
struct e97_cdw_5 {
	uint32_t ctx_ptr/*: 30*/;	/*!< The pointer to the offset of the Context in Host memory */
	//uint32_t refresh: 2;	/*!< Refresh flags to update out-of-date context */
};

struct e97_cdw {
	struct e97_cdw_0 dw0;	/*!<Command Descriptor word 0 */
	struct e97_cdw_1 dw1;	/*!<Command Descriptor word 1 */
	struct e97_cdw_2 dw2;	/*!<Command Descriptor word 2 */
	struct e97_cdw_3 dw3;	/*!<Command Descriptor word 3 */
	struct e97_cdw_4 dw4;	/*!<Command Descriptor word 4 */
	struct e97_cdw_5 dw5;	/*!<Command Descriptor word 5 */
} ;

struct ipsec_info {
    /*AVM specific*/
    u32 spi; /*!< Security Parameter Index identifiying corresponding Security Association */

    enum {
        INBOUND,
        OUTBOUND
    } dir; /*!< Direction: Inbound SAs must not encrypt, Outbound must not decrypt! */

    //TODO: request ID hinzufuegen welche state und policy verknuepft, damit wir
    //outbound wissen welcher SPI/SA fuer die erkannte notwendigkeit der IPSec-Verschluesselung
    //benutzt werden soll? denn der PAE wird verm. von den policies instruiert, welche nicht
    //den SPI kennen sondern nur die request ID desjenigen states mit dem SPI der SA.

    __be16 encap_sport; /*!< UDP Source Port for UDP Encapsulation */
    __be16 encap_dport; /*!< UDP Destination Port for UDP Encapsulation */
    enum {
        NONE,
        ESPINUDP,
        ESPINUDP_NONIKE
    } encap_type; /*!< UDP Encapsulation Type */

    __be32 src; /*!< Source IPv4 Address of outer IP header */
    __be32 dst; /*!< Destination IPv4 Address of outer IP header */

    /*EIP97 specific */
    struct e97_cdw 	cd_info; /*!< EIP97 CD(Command Descriptor Information)*/
    uint8_t  cd_size; 		 /*!< Command Descriptor Size in DWORD (4 bytes). If follow this implementation proposal, its size
                                                                is EIP97_CD_SIZE ,*/

    /*padding variables*/
    uint8_t pad_en; 				/*!< flag whether pad needed or not. For block ciphers , it must be set to 1, otherwise 0*/
    uint8_t pad_instr_offset;	/*!< the insert instruction offset (in bytes) in the ACD for padding. valid only if pad_en is 1.
                                                                            MPE FW need to update it accordingly if pad_en is set */
    uint8_t crypto_instr_offset;/*!< direction instruction offset (in bytes) in the ACD for crypto to encrypt/decrypt data */
    uint8_t blk_size; 		/*!< crypto block size for calculating padding length in bytes */

    /* AES-CCM specific */
    uint32_t hash_pad_instr_offset; /*!< insert instr offset (in bytes) in the ACD for adding hash padding length. \n
                                         Offset > 0 for AES-CCM mode and must be 0 for other modes. */
    uint32_t msg_len_instr_offset;  /*!< message length instr offset (in bytes) in the ACD. \n
                                         Offset > 0 for AES-CCM mode and must be 0 for other modes.  */

    /* Integrity Check Value (ICV) */
    uint8_t icv_len; 			/*!< ICV length in bytes. Used by FW for MTU sanity checking */

    /* instruction vectors (IV), check its source i.e from token, context record, or hardware generated randomly */
    uint8_t iv_len; 			/*!< IV length in bytes. Used by FW for MTU sanity checking */

};
#endif
