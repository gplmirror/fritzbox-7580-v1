/*
 * Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */		
#include <linux/err.h>
#include <linux/irqdomain.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/pinctrl/machine.h>
#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/pinctrl/pinconf.h>
#include <linux/pinctrl/pinconf-generic.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/irqchip/chained_irq.h>
#include <linux/spinlock.h>
#if defined(CONFIG_AVM_ENHANCED) && defined(CONFIG_PINCTRL_XRX500)
#include <asm/mach_avm.h>
#include <lantiq_soc.h>

/*--- #define DBG_TRC(args...) printk(KERN_INFO args) ---*/
#define DBG_TRC(args...)

#define PORTS			2
#define PINS			32
#define PORT(x)			(x / PINS)
#define PORT_PIN(x)		(x % PINS)

#define MUX_REG_OFF				0x100
#define MUX_BASE(p)				(MUX_REG_OFF * PORT(p))

#define DBG_ERR(args...) printk(KERN_ERR args)
/*--- #define DBG_TRC(args...) printk(KERN_INFO args) ---*/
#define DBG_TRC(args...)

static DEFINE_MUTEX(local_mutex);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _ltq_avm_pinctrl_priv {
    unsigned int ports;
	void __iomem *membase[PORTS];
    struct mutex *pmutex;
} ltq_avm_pinctrl;

/*--------------------------------------------------------------------------------*\
 * pad_offset1 0x80000, pad_offset2 0x80000 + 0x100
\*--------------------------------------------------------------------------------*/
struct _ltq_pad_reg_x {
    volatile unsigned int portmuxc[32];     /*--- 0x00 .. 0x7c ---*/
    volatile unsigned int puen;             /*--- 0x80 pullup enable ---*/
    volatile unsigned int pden;             /*--- 0x84 pulldown enable ---*/
    volatile unsigned int src;              /*--- 0x88 slew rate control ---*/
    volatile unsigned int dcc0;             /*--- 0x8c drive current control ---*/
    volatile unsigned int dcc1;             /*--- 0x90 drive current control ---*/
    volatile unsigned int od;               /*--- 0x94 od ---*/
    volatile unsigned int avail;            /*--- 0x98 pad control availability ---*/
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _gpio_mux_name {
	struct {
		const char *in;
		const char *out;
	} mux[4];
};

static struct _gpio_mux_name mux_names[] = {
	[ 0] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
		}
	},
	[ 1] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .out = "ssi_reset" },
		}
	},
	[ 2] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .in = "usb_oc1_int" },
		}
	},
	[ 3] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .in = "25MHz-input", .out = "25MHz-output" },
			[0b11] = { .in = "NTR-input" },
		}
	},
	[ 4] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "LED_ST" },
			[0b10] = { .out = "GPHY2_LED0" },
		}
	},
	[ 5] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "LED_D" },
			[0b10] = { .out = "GPHY3_LED0" },
		}
	},
	[ 6] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "LED_SH" },
			[0b10] = { .out = "GPHY4_LED0" },
		}
	},
	[ 7] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "CLKOUT1" },
			[0b11] = { .in = "USB_OC0_INT" },
		}
	},
	[ 8] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "CLKOUT0" },
			[0b11] = { .out = "GPHY2_LED1" },
		}
	},
	[ 9] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .out = "GPHY5_LED0" },
		}
	},
	[10] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "SPI1_TX" },
			[0b10] = { .out = "/SPI0_CS4" },
		}
	},
	[11] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "SPI1_RX" },
			[0b11] = { .out = "/SPI0_CS6" },
		}
	},
	/* 12 not used */
	[13] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_ALE" },
		}
	},
	[14] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .out = "SPI1_CS0" },
			[0b11] = { .out = "GPHY6F_LED0" },
		}
	},
	[15] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "/SPI0_CS1", .out = "/SPI0_CS1" },
		}
	},
	[16] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "/SPI0_RX", .out = "/SPI0_RX" },
		}
	},
	[17] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "/SPI0_TX", .out = "/SPI0_TX" },
		}
	},
	[18] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "SPI0_CLK", .out = "SPI0_CLK" },
		}
	},
	[19] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "SPI1_CLK" },
			[0b11] = { .out = "GPHY3_LED1" },
		}
	},
	/* 20 not used */
	[21] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "I2C_SDA", .in = "I2C_SDA" },
			[0b11] = { .in = "PCIe3_WK" },
		}
	},
	[22] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "I2C_SCL" },
		}
	},
	[23] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_CS1" },
		}
	},
	[24] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_CLE" },
		}
	},
	/* 25 not used */
	/* 26 not used */
	/* 27 not used */
	[28] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "GMII_TXD4" },
			[0b10] = { .out = "GPHY4_LED1" },
			[0b11] = { .out = "TDM_FSC", .in = "TDM_FSC" },
		}
	},
	[29] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "GMII_TXD5" },
			[0b10] = { .out = "GPHY5_LED1" },
			[0b11] = { .out = "TDM_DO", .in = "TDM_DO" },
		}
	},
	[30] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "GMII_TXD6" },
			[0b10] = { .out = "GPHY6F_LED1" },
			[0b11] = { .out = "TDM_DI", .in = "TDM_DI" },
		}
	},
	[31] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "GMII_TXD7" },
			[0b11] = { .out = "TDM_DCL", .in = "TDM_DCL" },
		}
	},
	[32] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "MDIO (PAE)" },
		}
	},
	[33] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "MDC (PAE)", .in = "MDC (PAE)" },
		}
	},
	[34] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .out = "SSI_TX" },
		}
	},
	[35] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .in = "SSI_RX" },
		}
	},
	[36] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b10] = { .in = "SSI_CLK" },
		}
	},
	/* 37 not used */
	/* 38 not used */
	/* 39 not used */
	/* 40 not used */
	/* 41 not used */
	[42] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "MDIO (GSWIP-L)", .out = "MDIO (GSWIP-L)" },
		}
	},
	[43] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "MDC (GSWIP-L)", .in = "MDC (GSWIP-L)" },
		}
	},
	/* 44 not used */
	/* 45 not used */
	/* 46 not used */
	/* 47 not used */
	[48] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .in = "NAND Ready_Busy" },
		}
	},
	[49] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_RE" },
		}
	},
	[50] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D1" },
		}
	},
	[51] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D0" },
		}
	},
	[52] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D2" },
		}
	},
	[53] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D7" },
		}
	},
	[54] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D6" },
		}
	},
	[55] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D5" },
		}
	},
	[56] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D4" },
		}
	},
	[57] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_D3" },
		}
	},
	/* 58 not used */
	[59] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_WE" },
		}
	},
	[60] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_WP" },
		}
	},
	[61] = {
		.mux = {
			[0b00] = { .in = "gpio", .out = "gpio" },
			[0b01] = { .out = "NAND_SE" },
			[0b10] = { .in = "Brightness_detect", .out = "Brightness_detect" },
			[0b11] = { .in = "PCIe1_WK" },
		}
	},
	/* 62 not used */
	/* 63 not used */
};
	
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_pinctrl_init(struct mutex *pmutex, int ports, void __iomem *iobase[]){
    struct _ltq_avm_pinctrl_priv *pltq_pinctrl_priv = &ltq_avm_pinctrl;
    unsigned int i;

    DBG_TRC("%s: ports=%d\n", __func__, ports);
    if(ports > ARRAY_SIZE(pltq_pinctrl_priv->membase)) {
        DBG_ERR("%s: failed\n", __func__);
        return -EINVAL;
    }
    if(pmutex) {
        pltq_pinctrl_priv->pmutex = pmutex;
    } else {
        pltq_pinctrl_priv->pmutex = &local_mutex;
    }
    for(i = 0; i < ports; i++) {
        pltq_pinctrl_priv->membase[i] = iobase[i];
        DBG_TRC("%s: membase[%d]=%p\n", __func__, i, iobase[i]);
    }
    mb();
    pltq_pinctrl_priv->ports = ports;
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_pinctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode) {
    struct _ltq_pad_reg_x *ppad_base;
	struct _ltq_avm_pinctrl_priv *pltq_pinctrl_priv = &ltq_avm_pinctrl;
    int port, pin;

    DBG_TRC("%s: gpio_pin=%d pin_mode=%d\n", __func__, gpio_pin, pin_mode);
    port = PORT(gpio_pin);
	if(port >= pltq_pinctrl_priv->ports) {
        DBG_ERR("%s: gpio_pin=%d - %s\n", __func__, gpio_pin, (pltq_pinctrl_priv->ports == 0) ? "uninitialized" : "invalid port");
		return -EINVAL;
	}
    pin       = PORT_PIN(gpio_pin); 
    ppad_base = (struct _ltq_pad_reg_x *)pltq_pinctrl_priv->membase[port];
    if((ltq_r32(&ppad_base->avail) & ( 1 << pin)) == 0) {
        DBG_ERR("%s: gpio_pin=%d(%d) - no pad available %x\n", __func__, gpio_pin, pin, ltq_r32(&ppad_base->avail));
        return -ENOTSUPP;
    }
    DBG_TRC("%s: PORTMUX: %p\n", __func__, &ppad_base->portmuxc[pin]);
	mutex_lock(pltq_pinctrl_priv->pmutex);
    ltq_w32(pin_mode, &ppad_base->portmuxc[pin]);
	mutex_unlock(pltq_pinctrl_priv->pmutex);

	return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_pinctrl_config(unsigned int gpio_pin, enum _hw_gpio_config param, unsigned int set) {
    struct _ltq_pad_reg_x *ppad_base;
	struct _ltq_avm_pinctrl_priv *pltq_pinctrl_priv = &ltq_avm_pinctrl;
    int port, pin;
    volatile unsigned int *regmem;
    unsigned int mask, val;

    DBG_TRC("%s: gpio_pin=%d param=%d set=%d\n", __func__, gpio_pin, param, set);
    port = PORT(gpio_pin);
	if(port >= pltq_pinctrl_priv->ports) {
        DBG_ERR("%s: gpio_pin=%d - %s\n", __func__, gpio_pin, (pltq_pinctrl_priv->ports == 0) ? "uninitialized" : "invalid port");
		return -EINVAL;
	}
    pin       = PORT_PIN(gpio_pin); 
    ppad_base = (struct _ltq_pad_reg_x *)pltq_pinctrl_priv->membase[port];
    if((ltq_r32(&ppad_base->avail) & ( 1 << pin)) == 0) {
        DBG_ERR("%s: gpio_pin=%d(%d) - no pad available %x\n", __func__, gpio_pin, pin, ltq_r32(&ppad_base->avail));
        return -ENOTSUPP;
    }
	switch (param) {
        case PINCONF_PARAM_DRIVE_CURRENT:
            regmem = (pin <= 15) ? &ppad_base->dcc0 : &ppad_base->dcc1; 
            mask   = 0x3 << ((pin % 16) * 2);
            set    = set << ((pin % 16) * 2);
            break;
        case PINCONF_PARAM_SLEW_RATE:
            regmem = &ppad_base->src;
            mask   = 0x1 << pin;
            set    = set << pin;
            break;

        case PINCONF_PARAM_PULLUP:
            regmem = &ppad_base->puen;
            mask   = 0x1 << pin;
            set    = set << pin;
            break;
        case PINCONF_PARAM_PULLDOWN:
            regmem = &ppad_base->pden;
            mask   = 0x1 << pin;
            set    = set << pin;
            break;
        case PINCONF_PARAM_OPEN_DRAIN:
            regmem = &ppad_base->od;
            mask   = 0x1 << pin;
            set    = set << pin;
            break;
        default:
            pr_err("%s: Invalid config param %d\n", __func__, param);
            return -ENOTSUPP;
	}

	mutex_lock(pltq_pinctrl_priv->pmutex);
    val  = ltq_r32(regmem);
    val &= ~mask;
    val |= set;
    ltq_w32(val, regmem);

    DBG_TRC("%s: avail=%x regmem: %p, val=%x mask=%x, set=%x\n", __func__, ltq_r32(&ppad_base->avail), regmem, val, mask, set);
	mutex_unlock(pltq_pinctrl_priv->pmutex);
	return 0;
}
EXPORT_SYMBOL(grx_avm_pinctrl_config);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_pinctrl_show_gpio(struct seq_file *seq, unsigned int gpio_pin) {
    struct _ltq_pad_reg_x *ppad_base;
	struct _ltq_avm_pinctrl_priv *pltq_pinctrl_priv = &ltq_avm_pinctrl;
    int port, pin, dir;
    volatile unsigned int *regmem;
    unsigned int drive;
    const char *muxname = NULL;

    port = PORT(gpio_pin);
	if(port >= pltq_pinctrl_priv->ports) {
        DBG_ERR("%s: gpio_pin=%d - %s\n", __func__, gpio_pin, (pltq_pinctrl_priv->ports == 0) ? "uninitialized" : "invalid port");
		return -EINVAL;
	}
    pin       = PORT_PIN(gpio_pin); 
    ppad_base = (struct _ltq_pad_reg_x *)pltq_pinctrl_priv->membase[port];
    if((ltq_r32(&ppad_base->avail) & ( 1 << pin)) == 0) {
        return -ENOTSUPP;
    }
    regmem    = (pin <= 15) ? &ppad_base->dcc0 : &ppad_base->dcc1; 
    drive     = ltq_r32(regmem) >> ((pin % 16) * 2);

    dir = grx_avm_gpio_get_dir(gpio_pin);
    if (dir == GPIO_INPUT_PIN) {
        muxname = mux_names[gpio_pin].mux[ltq_r32(&ppad_base->portmuxc[pin]) & 0x3].in;
    } else if (dir == GPIO_OUTPUT_PIN) {
        muxname = mux_names[gpio_pin].mux[ltq_r32(&ppad_base->portmuxc[pin]) & 0x3].out;
    }
    if(muxname == NULL) {
        muxname = "illegal";
    }
    seq_printf(seq, "%2d: mux=%d %-15s %s %s %s %s drive %s mA", gpio_pin, 
                            ltq_r32(&ppad_base->portmuxc[pin]), 
                            muxname,
                            ltq_r32(&ppad_base->od)   & ( 1 << pin) ? "OD" : "  ",
                            ltq_r32(&ppad_base->pden) & ( 1 << pin) ? "PD" : "  ", 
                            ltq_r32(&ppad_base->puen) & ( 1 << pin) ? "PU" : "  ", 
                            ltq_r32(&ppad_base->src)  & ( 1 << pin) ? "SLEW" : "    ", 
                            drive == 0 ? "2" : drive == 1 ? "4" : drive == 2 ? "8" : "12");
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void grx_avm_pinctrl_show(void) {
    unsigned int i;
    char buf[128];
    struct seq_file s;

    for(i = 0; i< 64; i++) {
        buf[0] = 0;
        memset(&s, 0, sizeof(s));
        s.buf  = buf;
        s.size = sizeof(buf);
        if(grx_avm_pinctrl_show_gpio(&s, i) == 0) {
            printk(KERN_ERR "%s\n", buf);
        }
    }
}
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) && defined(CONFIG_PINCTRL_XRX500) ---*/
