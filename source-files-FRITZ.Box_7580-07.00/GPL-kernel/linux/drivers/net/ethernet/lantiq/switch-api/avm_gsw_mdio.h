
#include <linux/kthread.h>  
#include <linux/list.h>
#include <linux/semaphore.h>
#include "gsw_init.h"

#define AVM_MDIO_READ 0
#define AVM_MDIO_WRITE 1


#define AVM_MDIO 0
#define AVM_MDIO_MMD 1

void avm_gsw_mdio_init(void);

GSW_return_t _GSW_MDIO_DataRead(void *cdev, GSW_MDIO_data_t *parm);
GSW_return_t _GSW_MDIO_DataWrite(void *cdev, GSW_MDIO_data_t *parm);
GSW_return_t avm_GSW_MDIO_DataRW(void *cdev, GSW_MDIO_data_t *parm, uint8_t rw, uint8_t avm_mdio);
