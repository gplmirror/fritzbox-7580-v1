#include <linux/atomic.h>
#include <linux/debugfs.h>
#include <linux/skbuff.h>
#include <linux/math64.h>

#include <linux/avm_eth_stats.h>

struct eth_stat_interval {
	unsigned int left_edge;
};

static const struct eth_stat_interval intervals[] = {
	{ .left_edge = 0 },
	{ .left_edge = 50 },
	{ .left_edge = 100 },
	{ .left_edge = 200 },
	{ .left_edge = 400 },
	{ .left_edge = 800 },
	{ .left_edge = 1000 },
	{ .left_edge = 1200 },
	{ .left_edge = 1515 },
	{ .left_edge = 3000 },
	{ .left_edge = 4000 },
	{ .left_edge = 6000 },
	{ .left_edge = 12000 },
	{ .left_edge = 15000 },
	{ .left_edge = 18000 },
	{ .left_edge = 21000 },
	{ .left_edge = 24000 },
	{ .left_edge = 30000 },
	{ .left_edge = 36000 },
	{ .left_edge = 42000 },
	{ .left_edge = 48000 },
	{ .left_edge = 54000 },
	{ .left_edge = 60000 },
	{ .left_edge = 64000 },
	{ .left_edge = 65536 },
};

static const char *eth_tracepoint_names[__ETH_TRACEPOINT_BOTTOM__];

static atomic_t results[__ETH_TRACEPOINT_BOTTOM__][ARRAY_SIZE(intervals)];

static atomic_t enabled = ATOMIC_INIT(0);

static atomic_t *get_counter_for_len(enum eth_tracepoint pos,
					    unsigned int len)
{
	unsigned int i;
	unsigned int max_size = ARRAY_SIZE(intervals);

	for (i = 0; i < max_size; i++) {
		if (i == max_size - 1 || len < intervals[i + 1].left_edge)
			return &results[pos][i];
	}

	BUG();
	return 0;
}

static void print_interval_name(struct seq_file *m, unsigned int i)
{
	const struct eth_stat_interval *interval = &intervals[i];

	if (i == ARRAY_SIZE(intervals) - 1)
		seq_printf(m, "[%5u, %5s)", interval->left_edge,
			   "-");
	else
		seq_printf(m, "[%5u, %5u)", interval->left_edge,
			   (interval + 1)->left_edge);
}

void clear_stats(void)
{
	memset(results, 0, sizeof(results));
}

void start_stats(void)
{
	atomic_set(&enabled, 1);
}

void stop_stats(void)
{
	atomic_set(&enabled, 0);
}

void avm_eth_stats_log(enum eth_tracepoint pos, size_t len)
{
	BUG_ON(pos < 0 || pos >= __ETH_TRACEPOINT_BOTTOM__);

	if (!atomic_read(&enabled))
		return;

	atomic_inc(get_counter_for_len(pos, len));
}
EXPORT_SYMBOL(avm_eth_stats_log);

static void print_stats_at_tracepoint(struct seq_file *m,
				      enum eth_tracepoint pos)
{
	unsigned int num_intervals = ARRAY_SIZE(intervals);
	unsigned int sum = 0;
	u64 sum_bytes = 0;
	atomic_t *tracepoint = results[pos];
	unsigned int i;

	seq_printf(m, "%s:\n", eth_tracepoint_names[pos]);

	for (i = 0; i < num_intervals; i++) {
		unsigned int counter = atomic_read(&tracepoint[i]);

		sum += counter;
		sum_bytes += ((u64)counter) * intervals[i].left_edge;
	}

	if (sum == 0) {
		seq_printf(m, "No packets\n");
		return;
	}

	for (i = 0; i < num_intervals; i++) {
		unsigned int counter = atomic_read(&tracepoint[i]);

		print_interval_name(m, i);
		seq_printf(m, ": %07u\t%02u%%\n", counter, counter * 100 / sum);
	}
	seq_printf(m, "---------------------------\nCount:          %07u\n",
		   sum);
	seq_printf(m, "Avg Size:       %07llu\n", div64_ul(sum_bytes, sum));
}

static char* get_usage(void)
{
	static char *usage = "usage: echo clear|start|stop|restart > eth_stats";

	return usage;
}

static void debugfs_eth_stats_output(struct seq_file *m,
				     void *data __maybe_unused)
{
	unsigned int i;

	seq_printf(m, "%s\n", get_usage());
	seq_printf(m, "Stat recording enabled: %u\n\n", atomic_read(&enabled));

	for (i = 0; i < ARRAY_SIZE(results); ++i) {
		print_stats_at_tracepoint(m, i);
		seq_printf(m, "\n");
	}
}

static int debugfs_eth_stats_input(char *input, void *priv_data __maybe_unused)
{
	static char *clear_cmd = "clear";
	static char *start_cmd = "start";
	static char *stop_cmd = "stop";
	static char *restart_cmd = "restart";

	if (!strncmp(clear_cmd, input, strlen(clear_cmd)))
		clear_stats();
	else if (!strncmp(start_cmd, input, strlen(start_cmd)))
		start_stats();
	else if (!strncmp(stop_cmd, input, strlen(stop_cmd)))
		stop_stats();
	else if (!strncmp(restart_cmd, input, strlen(restart_cmd))) {
		stop_stats();
		clear_stats();
		start_stats();
	} else {
		pr_err("%s\n", get_usage());
		return -EINVAL;
	}

	return 0;
}

int __init avm_eth_stats_init(void)
{
eth_tracepoint_names[ETH_TRACEPOINT_TCP_XMIT_PRE_SPLIT] =
	"tcp: Segment size popped from the send buffer before trim to window:";
eth_tracepoint_names[ETH_TRACEPOINT_DP_XMIT] =
	"eth: Frame size passed to dp_xmit of xrx500 driver";

	add_simple_debugfs_file("eth_stats", NULL, debugfs_eth_stats_input,
				debugfs_eth_stats_output, NULL);

	return 0;
}

late_initcall(avm_eth_stats_init);

