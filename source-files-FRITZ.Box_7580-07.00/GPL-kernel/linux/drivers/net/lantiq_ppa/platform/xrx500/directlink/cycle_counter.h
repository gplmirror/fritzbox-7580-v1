#ifndef __CYCLEX_COUNTER_H__
#define __CYCLEX_COUNTER_H__

//---------------------------------------------
//          #defines
//---------------------------------------------
extern void * CycleCounter_Create(char *);
extern void CycleCounter_Start(void *);
extern void CycleCounter_End(void *);

extern void *function1_cycles, *function2_cycles,*function3_cycles,*function4_cycles,*function5_cycles,*function6_cycles,*function7_cycles,*function8_cycles; 
extern void *functionA_cycles, *functionB_cycles,*functionC_cycles,*functionD_cycles;

/* above function1_cycles  and fucntion2_cycles  are pointer for function1 and function2 respectively */


#endif  // #ifndef __CYCLEX_COUNTER_H__
