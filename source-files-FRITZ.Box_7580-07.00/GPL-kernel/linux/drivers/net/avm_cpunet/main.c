/* We do not use ethernet headers and therefore need to remove the IP
 * alignment. Otherwise our IP header is unaligned.
 */
#define NET_IP_ALIGN 0

#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/if_arp.h>
#include <linux/debugfs.h>
#include <linux/version.h>
#include <linux/timer.h>

#include "avm_cpunet.h"

#define CPUNET_TX_QUEUE_LEN 50

struct dentry *avm_cpunet_debug_dir;

struct cpunet {
	struct napi_struct napi;
	struct timer_list retry_rx;
};

static struct net_device *cpunet_device;

static void timer_retry_rx(__maybe_unused unsigned long data)
{
	struct cpunet *cpunet = netdev_priv(cpunet_device);

	napi_schedule(&cpunet->napi);
}

static void retry_rx_later(void)
{
	struct cpunet *cpunet = netdev_priv(cpunet_device);

	mod_timer_pinned(&cpunet->retry_rx, jiffies + msecs_to_jiffies(100));
}

static int cpunet_poll(struct napi_struct *napi, int budget)
{
	int i;

	if (!cpunet_hw_tx_full())
		netif_wake_queue(cpunet_device);

	for (i = 0; i < budget; ++i) {
		const void *data;
		unsigned int len;
		struct sk_buff *skb;

		/* Do not remove from the queue -> just peek */
		if (cpunet_hw_rx(&data, &len))
			break;

#if KERNEL_VERSION(3, 19, 0) <= LINUX_VERSION_CODE
		skb = napi_alloc_skb(napi, len);
#else
		skb = netdev_alloc_skb_ip_align(cpunet_device, len);
#endif
		/* Out of memory -> block rx queue */
		if (unlikely(!skb)) {
			retry_rx_later();
			break;
		}

		/* Copy done -> make room in rx queue */
		memcpy(skb_put(skb, len), data, len);
		cpunet_hw_rx_done();

		/* We have no mac header and therefore and only allow IPv6 */
		skb->protocol = ETH_P_IPV6;
		skb_reset_mac_header(skb);

#if !IS_ENABLED(CONFIG_AVM_CPUNET_DEBUG_CHECKSUM)
		skb->ip_summed = CHECKSUM_UNNECESSARY;
#endif

		napi_gro_receive(napi, skb);

		/* No atomic increment necessary only written here */
		cpunet_device->stats.rx_packets++;
		cpunet_device->stats.rx_bytes += len;
	}

	if (i < budget) {
		napi_complete(napi);
		cpunet_hw_enable_int();
	}

	return i;
}

static enum irqreturn cpunet_isr(__maybe_unused int irq,
				 __maybe_unused void *data)
{
	struct cpunet *cpunet = netdev_priv(cpunet_device);

	cpunet_hw_disable_int();
	cpunet_hw_ack_int();
	napi_schedule(&cpunet->napi);
	return IRQ_HANDLED;
}

static netdev_tx_t cpunet_xmit(struct sk_buff *skb, struct net_device *dev)
{
	void *mem;
	int err;

	mem = cpunet_hw_tx_alloc();
	if (unlikely(!mem))
		return NETDEV_TX_BUSY;

	skb_orphan(skb);
	skb_copy_bits(skb, 0, mem, (int)skb->len);
	skb_tx_timestamp(skb);
	err = cpunet_hw_tx(mem, skb->len);
	BUG_ON(err);

	/* No atomic increment necessary only written here */
	dev->stats.tx_packets++;
	dev->stats.tx_bytes += skb->len;

	consume_skb(skb);

	if (cpunet_hw_tx_full())
		netif_stop_queue(dev);

	return NETDEV_TX_OK;
}

static int cpunet_up(__maybe_unused struct net_device *dev)
{
	return 0;
}

static int cpunet_down(__maybe_unused struct net_device *dev)
{
	return 0;
}

static void cpunet_unregister(struct net_device *dev)
{
	pr_info("%s: %s\n", __func__, dev->name);
}

static const struct net_device_ops cpunet_ops = {
	.ndo_open = &cpunet_up,
	.ndo_stop = &cpunet_down,
	.ndo_uninit = &cpunet_unregister,
	.ndo_start_xmit = &cpunet_xmit,
};

static void cpunet_setup(struct net_device *dev)
{
	dev->type = ARPHRD_NONE;
	dev->hard_header_len = 0;
	dev->addr_len = 0;
	dev->mtu = cpunet_hw_buffer_size();
	dev->tx_queue_len = CPUNET_TX_QUEUE_LEN;
	dev->flags = IFF_POINTOPOINT | IFF_NOARP;
	dev->priv_flags = IFF_TX_SKB_SHARING | IFF_DONT_BRIDGE;
	dev->features = NETIF_F_SG | NETIF_F_FRAGLIST | NETIF_F_HIGHDMA;

#if !IS_ENABLED(CONFIG_AVM_CPUNET_DEBUG_CHECKSUM)
	dev->features |= NETIF_F_HW_CSUM;
#endif

	dev->netdev_ops = &cpunet_ops;
}

static int __init cpunet_init(void)
{
	int err = -ENOMEM;
	struct cpunet *cpunet;

	cpunet_device =
		alloc_netdev(sizeof(struct cpunet), "cpunet", &cpunet_setup);
	if (!cpunet_device)
		goto out;

	cpunet = netdev_priv(cpunet_device);
	memset(cpunet, 0, sizeof(*cpunet));
	netif_napi_add(cpunet_device, &cpunet->napi, &cpunet_poll,
		       cpunet_hw_data_max_num());
	setup_timer(&cpunet->retry_rx, timer_retry_rx, 0);

	err = register_netdev(cpunet_device);
	if (err)
		goto netdev_dealloc;

	avm_cpunet_debug_dir = debugfs_create_dir("avm_cpunet", NULL);
	if (!avm_cpunet_debug_dir)
		goto netdev_unregister;

	err = cpunet_hw_init(&cpunet_isr);
	if (err)
		goto debug_fs;

	napi_enable(&cpunet->napi);
	netif_start_queue(cpunet_device);

	return 0;

debug_fs:
	debugfs_remove_recursive(avm_cpunet_debug_dir);
netdev_unregister:
	unregister_netdev(cpunet_device);
netdev_dealloc:
	free_netdev(cpunet_device);
out:
	return err;
}
module_init(cpunet_init);

static void __exit cpunet_exit(void)
{
	struct cpunet *cpunet = netdev_priv(cpunet_device);

	netif_tx_disable(cpunet_device);
	napi_disable(&cpunet->napi);
	del_timer_sync(&cpunet->retry_rx);
	cpunet_hw_exit();
	netif_napi_del(&cpunet->napi);
	unregister_netdev(cpunet_device);
	free_netdev(cpunet_device);
	debugfs_remove_recursive(avm_cpunet_debug_dir);
}
module_exit(cpunet_exit);

MODULE_AUTHOR("Alexander Theißen <a.theissen@avm.de>");
MODULE_DESCRIPTION("Cross CPU Network Device");
MODULE_LICENSE("GPL");
