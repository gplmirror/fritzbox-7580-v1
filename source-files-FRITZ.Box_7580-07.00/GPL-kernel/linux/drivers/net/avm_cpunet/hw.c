#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/debugfs.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/of.h>
#include <asm/setup.h>
#include <irq.h>
#include <lantiq.h>
#include <lantiq_irq.h>
#ifdef CONFIG_SOC_GRX500_BOOTCORE
#include <grx500_bootcore_cnfg.h>
#include <grx500_bootcore_chadr.h>
#include <grx500_bootcore_chipreg.h>
#include <grx500_bootcore_defs.h>
#include <grx500_bootcore_emerald_env_regs.h>
#include <grx500_bootcore_interrupt.h>
#include <grx500_bootcore_time.h>
#include <grx500_bootcore_uart.h>
#endif

#include "avm_cpunet.h"

/* registers */
#define MPS_REG_BASE_ADDR 0x1f107400
#define MPS_REG_SIZE 0x33f
#define MPS_RAD0SR ((u32 *)(mps_reg_mapping + 0x0040))
#define MPS_RAD1SR ((u32 *)(mps_reg_mapping + 0x0084))
#define MPS_SAD0SR ((u32 *)(mps_reg_mapping + 0x0048))
#define MPS_SAD1SR ((u32 *)(mps_reg_mapping + 0x0070))
#define MPS_CAD0SR ((u32 *)(mps_reg_mapping + 0x0050))
#define MPS_CAD1SR ((u32 *)(mps_reg_mapping + 0x0080))
#define MPS_AD0ENR ((u32 *)(mps_reg_mapping + 0x0058))
#define MPS_AD1ENR ((u32 *)(mps_reg_mapping + 0x0074))

/* Interrupt vectors */
#define MPS_IR4 /* AD0 */ 22u
#define MPS_IR0 /* Global interrupt */ 223u

/* We only use the first bit of the MPS registers */
#define MPS_IRQ_MASK 0x1

/* Mailbox area (shared uncached sram to deliver short messages) */
#define MPS_MAILBOX_ADDR 0x1f201000
#define MPS_MAILBOX_SIZE 1024u
#define MPS_MAILBOX_CTRL_NUM 16u
#define MPS_MAILBOX_DATA_NUM 16u

/* Memory allocator (for packet payload) */
#define MPS_MEMPOOL_CHUNK_SIZE (1 << 13)
#define MPS_MEMPOOL_SIZE (MPS_MEMPOOL_CHUNK_SIZE * MPS_MAILBOX_DATA_NUM)
#define MPS_MEMPOOL_NAME_BOOTCORE "avm_cpunet_tep"
#define MPS_MEMPOOL_NAME_INTERAPTIV "avm_cpunet_int"
#ifdef CONFIG_SOC_GRX500_BOOTCORE
#define MPS_MEMPOOL_NAME_SELF MPS_MEMPOOL_NAME_BOOTCORE
#define MPS_MEMPOOL_NAME_OTHER MPS_MEMPOOL_NAME_INTERAPTIV
#else
#define MPS_MEMPOOL_NAME_SELF MPS_MEMPOOL_NAME_INTERAPTIV
#define MPS_MEMPOOL_NAME_OTHER MPS_MEMPOOL_NAME_BOOTCORE
#endif

#ifdef CONFIG_SOC_GRX500_BOOTCORE
#define MPS_CACHE_LINE_SZ 16u
#else
#define MPS_CACHE_LINE_SZ 32u
#endif
#define MPS_CACHE_LINE_MASK (~(MPS_CACHE_LINE_SZ - 1))

struct mps_message {
	u32 msg;
};

struct queue_shared {
	u32 read;
	u32 write;
	struct mps_message buffer[];
};

/* queue_shared and mps_message are the only on-the-wire structs and therefore
 * need to have a consistent padding (i.e zero padding) on both CPUs. We also
 * implicitly assume that both CPUs use the same endianess. Because we cannot
 * easily check that this is the case we just check that they are both big
 * endian. In this way we get at least an error when this changes.
 */
static void __maybe_unused check_zero_padding_and_endianess(void)
{
	u8 swaptest[] = { 1, 0, 0, 0 };

	BUILD_BUG_ON_MSG(sizeof(struct queue_shared) != 2 * sizeof(u32),
			 "struct queue_shared must be zero padded");
	BUILD_BUG_ON_MSG(sizeof(struct mps_message) != sizeof(u32),
			 "struct mps_message must be zero padded");
	BUILD_BUG_ON_MSG(
		*(u32 *)swaptest == 1,
		"Little endian detected. Make sure that both CPUs use the same endianess.");

	BUILD_BUG_ON(!is_power_of_2(MPS_MAILBOX_CTRL_NUM));
	BUILD_BUG_ON(!is_power_of_2(MPS_MAILBOX_DATA_NUM));
}

/* One queue defines one distinct data area to pass messages between CPUs.
 * The shared part lies inside SRAM and is read/write by both CPUs.
 * The capacity is positioned in private memory to prevent the other
 * CPU from tampering with the bound checks.
 */
struct queue {
	const unsigned int capacity;
	struct queue_shared *shared;
};

/* Virtual address of our registers */
static void __iomem *mps_reg_mapping;

static void *mps_mailbox_mapping;
static struct queue mps_data_read = { .capacity = MPS_MAILBOX_DATA_NUM - 1 };
static struct queue mps_ctrl_read = { .capacity = MPS_MAILBOX_CTRL_NUM - 1 };
static struct queue mps_data_write = { .capacity = MPS_MAILBOX_DATA_NUM - 1 };
static struct queue mps_ctrl_write = { .capacity = MPS_MAILBOX_CTRL_NUM - 1 };

/* The buffers to store the packet payload. The self mempool is for tx packets
 * and is read only by the other CPU. The other mempool is read only by this
 * CPU and is used to rx packets. They lie in reserved DDR ranges visible
 * to both CPUs.
 */
static u8 *mps_mempool_self;
static u8 *mps_mempool_other;

/******************************************************************************
 *
 * Static functions
 *
 ******************************************************************************/
static int queue_is_full(struct queue const *Q)
{
	unsigned int read = Q->shared->read;
	unsigned int write = Q->shared->write;

	if (((write + 1) & Q->capacity) == read)
		return -EAGAIN;

	/* This either is a bug or an attack. */
	BUG_ON(write > Q->capacity);

	return write;
}

static int queue_is_empty(struct queue const *Q)
{
	unsigned int read = Q->shared->read;
	unsigned int write = Q->shared->write;

	if (read == write)
		return -EAGAIN;

	/* This either is a bug or an attack. */
	BUG_ON(read > Q->capacity);

	return read;
}

static unsigned int queue_shared_size(struct queue const *Q)
{
	return sizeof(struct queue_shared) +
		(Q->capacity + 1) * sizeof(struct mps_message);
}

static unsigned int queue_count(struct queue const *Q)
{
	unsigned int read = Q->shared->read;
	unsigned int write = Q->shared->write;
	unsigned int ret;

	if (write >= read)
		ret = write - read;
	else
		ret = Q->capacity + 1 + write - read;

	return ret;
}

static int queue_peek(struct queue const *Q, struct mps_message *E)
{
	int read = queue_is_empty(Q);

	if (read < 0)
		return read;

	/* prevent Q->Buffer() being read before queue empty check */
	rmb();

	*E = Q->shared->buffer[read];

	return read;
}

static void queue_drop(struct queue *Q)
{
	/* there might be reads from the queue that must finish first */
	rmb();

	Q->shared->read = (Q->shared->read + 1) & Q->capacity;
}

static int queue_out(struct queue *Q, struct mps_message *E)
{
	int read = queue_peek(Q, E);

	if (read < 0)
		return read;

	queue_drop(Q);
	return read;
}

static int queue_in(struct queue *Q, struct mps_message const *E)
{
	int write = queue_is_full(Q);

	if (unlikely(write < 0))
		return write;

	/* prevent Q->Buffer() being written before the pointer is read
	 * by the queue_is_full check
	 */
	rmb();

	Q->shared->buffer[write] = *E;

	/* data must be written before pointers */
	wmb();

	Q->shared->write = (write + 1) & Q->capacity;
	return 0;
}

static void trigger_int(void)
{
#ifndef CONFIG_SOC_GRX500_BOOTCORE
	ltq_w32(MPS_IRQ_MASK, MPS_SAD0SR);
#else
	ltq_w32(MPS_IRQ_MASK, MPS_SAD1SR);
#endif
}

static void cache_inv(const void *addr, uint32_t len)
{
	uintptr_t aline = (uintptr_t)addr & MPS_CACHE_LINE_MASK;
	uintptr_t end = ((uintptr_t)addr + len) & MPS_CACHE_LINE_MASK;

	do {
		__asm__ __volatile__(" .set    push\n"
				     " .set    noreorder\n"
				     " .set    mips3\n\t\n"
				     " cache   0x11, 0(%0)\n"
				     " .set    pop\n"
				     :
				     : "r"(aline));
/* bootcore has no level 2 cache */
#ifndef CONFIG_SOC_GRX500_BOOTCORE
		__asm__ __volatile__(" .set    push\n"
				     " .set    noreorder\n"
				     " .set    mips3\n\t\n"
				     " cache   0x13, 0(%0)\n"
				     " .set    pop\n"
				     :
				     : "r"(aline));
#endif
		aline += MPS_CACHE_LINE_SZ;
	} while (aline <= end);
}

static void cache_wb_inv(const void *addr, uint32_t len)
{
	uintptr_t aline = (uintptr_t)addr & MPS_CACHE_LINE_MASK;
	uintptr_t end = ((uintptr_t)addr + len) & MPS_CACHE_LINE_MASK;

	/* All writes must hit cache before writing it to RAM. */
	wmb();

	do {
		__asm__ __volatile__(" .set    push\n"
				     " .set    noreorder\n"
				     " .set    mips3\n\t\n"
				     " cache   0x15, 0(%0)\n"
				     " .set    pop\n"
				     :
				     : "r"(aline));
/* bootcore has no level 2 cache */
#ifndef CONFIG_SOC_GRX500_BOOTCORE
		__asm__ __volatile__(" .set    push\n"
				     " .set    noreorder\n"
				     " .set    mips3\n\t\n"
				     " cache   0x17, 0(%0)\n"
				     " .set    pop\n"
				     :
				     : "r"(aline));

#endif
		aline += MPS_CACHE_LINE_SZ;
	} while (aline <= end);
}

static const void *calc_rx_buffer_addr(unsigned int read_index)
{
	return mps_mempool_other + (read_index * MPS_MEMPOOL_CHUNK_SIZE);
}

static void debugfs_status_out(struct seq_file *m, __maybe_unused void *priv)
{
	seq_printf(m, "tx ctrl: %02d/%02d\n", queue_count(&mps_ctrl_write),
		   cpunet_hw_ctrl_max_num());
	seq_printf(m, "rx ctrl: %02d/%02d\n", queue_count(&mps_ctrl_read),
		   cpunet_hw_ctrl_max_num());

	seq_printf(m, "tx data: %02d/%02d\n", queue_count(&mps_data_write),
		   cpunet_hw_data_max_num());
	seq_printf(m, "rx data: %02d/%02d\n", queue_count(&mps_data_read),
		   cpunet_hw_data_max_num());
}

/******************************************************************************
 *
 * Non-Static
 *
 ******************************************************************************/
unsigned int cpunet_hw_buffer_size(void)
{
	return MPS_MEMPOOL_CHUNK_SIZE;
}

unsigned int cpunet_hw_ctrl_max_num(void)
{
	return MPS_MAILBOX_CTRL_NUM - 1;
}

unsigned int cpunet_hw_data_max_num(void)
{
	return MPS_MAILBOX_DATA_NUM - 1;
}

void *cpunet_hw_tx_alloc(void)
{
	u8 *result;
	unsigned int write;

	if (unlikely(cpunet_hw_tx_full()))
		return NULL;

	write = mps_data_write.shared->write;

	/* This might be an attack. */
	BUG_ON(write > mps_data_write.capacity);

	result = mps_mempool_self + (write * MPS_MEMPOOL_CHUNK_SIZE);
	return result;
}

int cpunet_hw_tx(void *addr, unsigned int length)
{
	struct mps_message msg;
	int err;

	BUG_ON(length == 0 || length > cpunet_hw_buffer_size());

	/* make sure payload hits sdram before signalling */
	cache_wb_inv(addr, length);

	msg.msg = (u32)length;

	err = queue_in(&mps_data_write, &msg);
	if (unlikely(err))
		return err;

	trigger_int();
	return 0;
}

bool cpunet_hw_tx_full(void)
{
	return queue_is_full(&mps_data_write) < 0;
}

int cpunet_hw_rx(const void **data, unsigned int *len)
{
	struct mps_message msg;
	int read = queue_peek(&mps_data_read, &msg);

	if (read < 0)
		return read;

	/* This might be an attack. */
	BUG_ON(msg.msg > MPS_MEMPOOL_CHUNK_SIZE);

	*data = calc_rx_buffer_addr(read);
	*len = msg.msg;

	cache_inv(*data, *len);

	return 0;
}

void cpunet_hw_rx_done(void)
{
	queue_drop(&mps_data_read);
	trigger_int();
}

void *cpunet_hw_read_ctrl(void)
{
	struct mps_message msg;

	if (queue_out(&mps_ctrl_read, &msg) < 0)
		return NULL;
	else
		return (void *)(uintptr_t)msg.msg;
}

void cpunet_hw_ack_int(void)
{
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	ltq_w32(MPS_IRQ_MASK, MPS_CAD0SR);
#else
	ltq_w32(MPS_IRQ_MASK, MPS_CAD1SR);
#endif
}

void cpunet_hw_enable_int(void)
{
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	ltq_w32(MPS_IRQ_MASK, MPS_AD0ENR);
#else
	ltq_w32(MPS_IRQ_MASK, MPS_AD1ENR);
#endif
}

void cpunet_hw_disable_int(void)
{
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	ltq_w32(0x0, MPS_AD0ENR);
#else
	ltq_w32(0x0, MPS_AD1ENR);
#endif
}

/******************************************************************************
 *
 * Init
 *
 ******************************************************************************/
static int __init init_mailbox_fifos(void)
{
	void *ptr = mps_mailbox_mapping;
	uintptr_t size;

/* init pointers without writing to the address */
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	mps_ctrl_read.shared = ptr;
	ptr += queue_shared_size(&mps_ctrl_read);
	mps_data_read.shared = ptr;
	ptr += queue_shared_size(&mps_data_read);

	mps_ctrl_write.shared = ptr;
	ptr += queue_shared_size(&mps_ctrl_write);
	mps_data_write.shared = ptr;
	ptr += queue_shared_size(&mps_data_write);
#else
	mps_ctrl_write.shared = ptr;
	ptr += queue_shared_size(&mps_ctrl_write);
	mps_data_write.shared = ptr;
	ptr += queue_shared_size(&mps_data_write);

	mps_ctrl_read.shared = ptr;
	ptr += queue_shared_size(&mps_ctrl_read);
	mps_data_read.shared = ptr;
	ptr += queue_shared_size(&mps_data_read);
#endif
	/* verify that we are inside our SRAM bounds */
	size = ptr - mps_mailbox_mapping;
	pr_info("verfified SRAM fifos config using %lu/%u bytes of SRAM at 0x%08lx\n",
		size, MPS_MAILBOX_SIZE, (uintptr_t)mps_mailbox_mapping);
	if (size > MPS_MAILBOX_SIZE)
		return -1;

	/* only bootcore initializes memory as it boots first */
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	memset(mps_mailbox_mapping, 0, MPS_MAILBOX_SIZE);
#endif
	return 0;
}

static phys_t __init find_mempool_by_name(const char *name, u32 *len)
{
	struct device_node *reserved;
	struct property *prop;
	int prop_len;
	const __be32 *iter;
	u32 addr;

	reserved = of_find_node_by_path("/reserved-memory");
	if (!reserved)
		goto out;

	prop = of_find_property(reserved, name, &prop_len);
	if (!prop)
		goto reserved;

	iter = of_prop_next_u32(prop, NULL, &addr);
	if (!iter)
		goto reserved;

	iter = of_prop_next_u32(prop, iter, len);
	if (!iter)
		goto reserved;

/* Bootcore addresses start at 0x0... and not at 0x2... */
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	addr &= 0x0FFFFFFF;
#endif

	return addr;

reserved:
	of_node_put(reserved);
out:
	return 0;
}

static int __init init_mempool(void)
{
	phys_t self_addr, other_addr;
	u32 self_len, other_len;

	self_addr = find_mempool_by_name(MPS_MEMPOOL_NAME_SELF, &self_len);
	other_addr = find_mempool_by_name(MPS_MEMPOOL_NAME_OTHER, &other_len);

	if (!self_addr || !other_addr) {
		pr_err("Could not find reserved memory entries: %s, %s\n",
		       MPS_MEMPOOL_NAME_SELF, MPS_MEMPOOL_NAME_OTHER);
		return -EFAULT;
	}

	if (self_addr < MPS_MEMPOOL_SIZE || other_addr < MPS_MEMPOOL_SIZE) {
		pr_err("Reserved memory area too small. Needed=%u %s=%u %s=%u\n",
		       MPS_MEMPOOL_SIZE, MPS_MEMPOOL_NAME_SELF, self_len,
		       MPS_MEMPOOL_NAME_OTHER, other_len);
		return -EFAULT;
	}

	mps_mempool_self = phys_to_virt(self_addr);
	mps_mempool_other = phys_to_virt(other_addr);
	memset(mps_mempool_self, 0, MPS_MEMPOOL_SIZE);
	cache_wb_inv(mps_mempool_self, MPS_MEMPOOL_SIZE);
	cache_inv(mps_mempool_other, MPS_MEMPOOL_SIZE);

	pr_info("Self  mempool at [0x%08lx-0x%08lx) (%u Bytes)\n",
		(uintptr_t)mps_mempool_self,
		(uintptr_t)mps_mempool_self + MPS_MEMPOOL_SIZE,
		MPS_MEMPOOL_SIZE);

	pr_info("Other mempool at [0x%08lx-0x%08lx) (%u Bytes)\n",
		(uintptr_t)mps_mempool_other,
		(uintptr_t)mps_mempool_other + MPS_MEMPOOL_SIZE,
		MPS_MEMPOOL_SIZE);

	return 0;
}

static int __init init_debugfs(void)
{
	add_simple_debugfs_file("status", avm_cpunet_debug_dir, NULL,
				debugfs_status_out, NULL);

	return 0;
}

#ifdef CONFIG_SOC_GRX500_BOOTCORE
static struct irqaction mps_irqaction = {
	.handler = NULL, /* will be set on init */
	.flags = IRQF_DISABLED, /* disable nested interrupts */
	.name = "avm_cpunet",
	.dev_id = NULL,
};
#endif

void __exit cpunet_hw_exit(void)
{
	cpunet_hw_disable_int();
#ifdef CONFIG_SOC_GRX500_BOOTCORE
	grx500_bootcore_unregister_irq(MPS_IR4, 11, NULL);
#endif
	free_irq(MPS_IR0, NULL);
	iounmap(mps_mailbox_mapping);
	iounmap(mps_reg_mapping);
}

int __init cpunet_hw_init(irq_handler_t clb)
{
	int err = -1;
	unsigned int virt;

	mps_reg_mapping = ioremap(MPS_REG_BASE_ADDR, MPS_REG_SIZE);
	if (!mps_reg_mapping)
		goto err;

	mps_mailbox_mapping = ioremap(MPS_MAILBOX_ADDR, MPS_MAILBOX_SIZE);
	if (!mps_mailbox_mapping)
		goto reg;

	err = init_mailbox_fifos();
	if (err)
		goto mailbox;

	err = init_mempool();
	if (err)
		goto mailbox;

#ifdef CONFIG_SOC_GRX500_BOOTCORE
	virt = MPS_IR4;
	mps_irqaction.handler = clb;
	grx500_bootcore_register_static_irq(virt,
					    GRX500_BOOTCORE_MPS2_OUT_INDEX,
					    &mps_irqaction,
					    grx500_bootcore_mps_irq);
#else
	virt = MPS_IR0;
	err = request_irq(virt, clb, 0, "avm_cpunet", NULL);
	if (err)
		goto mailbox;
#endif

	cpunet_hw_enable_int();

	init_debugfs();

	pr_info("hw init successful\n");

	return 0;

mailbox:
	iounmap(mps_mailbox_mapping);
reg:
	iounmap(mps_reg_mapping);
err:
	pr_err("hw init failed\n");
	return err;
}
