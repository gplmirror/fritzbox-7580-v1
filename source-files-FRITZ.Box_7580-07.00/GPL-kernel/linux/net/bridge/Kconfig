#
# 802.1d Ethernet Bridging
#

config BRIDGE
	tristate "802.1d Ethernet Bridging"
	select LLC
	select STP
	---help---
	  If you say Y here, then your Linux box will be able to act as an
	  Ethernet bridge, which means that the different Ethernet segments it
	  is connected to will appear as one Ethernet to the participants.
	  Several such bridges can work together to create even larger
	  networks of Ethernets using the IEEE 802.1 spanning tree algorithm.
	  As this is a standard, Linux bridges will cooperate properly with
	  other third party bridge products.

	  In order to use the Ethernet bridge, you'll need the bridge
	  configuration tools; see <file:Documentation/networking/bridge.txt>
	  for location. Please read the Bridge mini-HOWTO for more
	  information.

	  If you enable iptables support along with the bridge support then you
	  turn your bridge into a bridging IP firewall.
	  iptables will then see the IP packets being bridged, so you need to
	  take this into account when setting up your firewall rules.
	  Enabling arptables support when bridging will let arptables see
	  bridged ARP traffic in the arptables FORWARD chain.

	  To compile this code as a module, choose M here: the module
	  will be called bridge.

	  If unsure, say N.

config AVM_BRIDGE_FLOOD_RATELIMITER
	bool "Port flood rate limiting"
	depends on AVM_ENHANCED
	depends on BRIDGE
	default y
	---help---
	  AVM extension

	  This enables a simple, configurable bridge flood rate limiting mechanism, to
	  help protect accidental or willful network loops. You can configure a
	  maximum packet rate and a reserve. The reserve can be higher to allow for
	  short bursts.

	  The ratelimiter will only limit packets which are forwarded on all ports.
	  Traffic (unicast and multicast) that is forwarded to specific ports is
	  unaffected. Locally generated traffic is unaffected as well. It is
	  disabled by default and must be configured through sysfs.

config BRIDGE_IGMP_SNOOPING
	bool "IGMP/MLD snooping"
	depends on BRIDGE
	depends on INET
	default y
	---help---
	  If you say Y here, then the Ethernet bridge will be able selectively
	  forward multicast traffic based on IGMP/MLD traffic received from
	  each port.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.

config AVM_BRIDGE_MULTICAST_TO_UNICAST
	bool "bridge multicast to unicast conversion"
	depends on BRIDGE_IGMP_SNOOPING
	default y
	---help---
	  AVM extension

	  If you say Y here, then the Ethernet bridge ports record the source
	  MAC addresses of IGMP report senders, and perform a multicast-to-unicast
	  conversion (Layer 2) before forwarding corresponding multicast packets. This
	  is useful in wireless networks where multicast traffic is severely limited
	  by 802.11.

	  Even with this being configured, the feature must be turned on on a per-port
	  basis at runtime.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.

config AVM_BRIDGE_MULTICAST_TO_UNICAST_DEFAULT_THRESHOLD
	int "multicast to unicast default threshold"
	depends on AVM_BRIDGE_MULTICAST_TO_UNICAST
	default 3
	---help---
	  AVM extension

	  This selects the default threshold for switching from multicast-as-unicast
	  transmission back to plain multicast. This is really only the default,
	  the threshold can be configured on a per port basis on sysfs. The threshold
	  is multicast group specific.


config AVM_BRIDGE_MULTICAST_TO_UNICAST_NO_SSDP
	bool "multicast to unicast excption for well known SSDP multicasts"
	depends on AVM_BRIDGE_MULTICAST_TO_UNICAST
	default y
	---help---
	  AVM extension

	  This selects the exception for SSDP multicasts in the multicast-to-unicast-feature.
	  To avoid problems with SSDP say Y here because there are a lot of
	  multicast senders and receipients inside the home net normally.


config BRIDGE_VLAN_FILTERING
	bool "VLAN filtering"
	depends on BRIDGE
	depends on VLAN_8021Q
	default n
	---help---
	  If you say Y here, then the Ethernet bridge will be able selectively
	  receive and forward traffic based on VLAN information in the packet
	  any VLAN information configured on the bridge port or bridge device.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.


config AVM_BRIDGE_ISOLATION
	bool "Isolate bridge ports from each other"
	depends on BRIDGE && NETFILTER
	depends on SYSFS
	depends on AVM_ENHANCED
	default y
	---help---
	  AVM extension

	  This feature allows to create isolated bridge port pairs, so that clients
	  connected through an isolated port cannot communicate with clients
	  behind other isolated ports. Traffic flowing from/to non-isolated ports
	  is not affected.

config LTQ_MCAST_PROXY
        tristate "MCAST Proxy support"
        depends on m
        default n
        help
          Lantiq Multicast Proxy support

config LTQ_MCAST_SNOOPING
        tristate "MCAST Snooping support"
        default y
        help
          Lantiq Multicast Snooping support

config WAVE_WIFI_PROXYARP
        boolean "WAVE WiFi ProxyARP support"
        default n
        help
          WAVE WiFi ProxyARP support
