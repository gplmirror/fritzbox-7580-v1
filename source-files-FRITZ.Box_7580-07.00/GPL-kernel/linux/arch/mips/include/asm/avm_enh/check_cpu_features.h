#ifndef __check_cpu_features_h__
#define __check_cpu_features_h__

void avm_check_cpu_features(void);
void avm_check_isa_features(void);
void avm_check_pcache_features(void);
void avm_check_scache_features(void);

#endif
